#!/bin/env ruby
# encoding: utf-8
class RatingsController < ApplicationController
  def send_rating_mail
    @issue = Issue.find params[:id]

    if !@issue.rating.blank?
      flash[:error] = "Оценка по задаче уже выставлена и не может быть запрошена повторно"
    elsif @issue.ratings.any?&&(@issue.ratings.first.created_on+3)>Date.current
      flash[:error] = "Оценка по задаче запрашивалась менее 3х дней назад"
    elsif @issue.client_mail
      @issue.gen_ratings
      begin
        Mailer.get_rating(@issue).deliver
        flash[:notice] = 'Запрос оценки успешно направлен заказчику'
      rescue
        flash[:error] = 'Во время отправки запроса оценки произошел сбой, проверьте корректность почтового адреса клиента и попробуйте еще раз'
        @issue.ratings.each {|r| r.destroy}
      end
    else
      flash[:error] = 'Либо Исполнитель должен быть клиентом, либо в поле Клиент - значение в формате "...<<email>>"'
    end
    redirect_to @issue
  end

  def set_rating
    if (rating = Rating.find_by_key(params[:key]))
      @issue = rating.issue
      @issue.rating = rating.rating
      @issue.ratings.each {|r| r.destroy}
      render 'thanks_for_rating'
    else
      render 'not_found'
    end
  end

  def report
    @project = Project.find(params['project_id'])
    date_from = params[:date_from]
    date_to = params[:date_to]
    template = 'report_rating'

    pr={}

    pr.merge! 'issue' => :master
    @issues = @project.issues.where("due_date between '#{date_from}' and '#{date_to}'").order(:id).all


    generate_rtf_by_template pr, "#{template}.rtf", "#{template}-#{@project.identifier}.rtf"
  end


  private

  TEMPLATESPATH = "#{PLUGIN_PATH}/templates/"

  def generate_rtf_by_template params, file_name_input, file_name_output
    file_name_input  = TEMPLATESPATH + file_name_input

    # Загружаем шаблон
    begin
      template  = File.open(file_name_input,"r") {|file|file.read}
    rescue BlogDataNotFound
      STDERR.puts "Файл #{file_name_input} не найден"
    rescue Exception => exc
      STDERR.puts " Общая ошибка загрузки #{file_name_input}: #{exc.message}"
    end

    temp = [[],[],[],[],[]]
    u    = ['','','','','']
    a = template.split(/(<%start\(\w+\)%>|<%end%>)/)
    k = 0
    del_par = false
    a.map do |s|
      if s =~ /<%start\((\w+)\)%>/
        k = k+1
        u[k] = $1
        temp[k] = []
        del_par = true
      elsif s =~ /<%end%>/
        temp[k-1] << {u[k].to_s => temp[k]}
        k=k-1
        del_par = true
      else
        if del_par
          s.sub!('\par ',' ')
        end
        temp[k] << s
        del_par = false
      end
    end
    a = temp[0]

    @output = ''
    replace_rtf a, params
    @output = @output.recode_for_rtf

    send_data @output,
              :filename => file_name_output,
              :type => 'application/msword',
              :disposition => 'attachment'
  end

  def replace_rtf(ar, params)
    ar.each do |s|
      if s.class == String
        t=String.new(s)
        t.gsub!(/<%(\w+)%>/)          { params[$1].to_s.gsub(/\n/,'\\par ') }
        t.gsub!(/<%(\w+)\.(\w+)%>/)   { self.instance_variable_get("@#{$1}").send($2).to_s.gsub(/\n/){'\\par '} }
        t.gsub!(/<%(\w+)\[(\w+)\]%>/) { self.instance_variable_get("@#{$1}").call(self.instance_variable_get("@#{$2}")) }
        @output << t
      else
        s.each do |list, sub_a|
          if params[list] == :detail
            self.instance_variable_get("@#{list}_detail").call
          end

          self.instance_variable_get("@#{list}s").each do |item|
            self.instance_variable_set("@#{list}", item)
            replace_rtf sub_a, params
          end
        end
      end
    end
  end


end