# frozen_string_literal: true

module HomnetSettingsHelper
  def plugin_setting_select_from_collection(setting, choices, options = {})
    setting_label(setting, options).html_safe +
      select_tag("settings[#{setting}]",
                 options_from_collection_for_select(choices, 'id', 'name', Setting.send("plugin_#{@plugin.id}")[setting.to_s]),
                 options).html_safe
  end

  def plugin_setting_multiselect(setting, choices, options = {})
    setting_values = Setting.send("plugin_#{@plugin.id}")[setting]
    setting_values = [] unless setting_values.is_a?(Array)

    content_tag('label', l(options[:label] || "setting_#{setting}")) +
      hidden_field_tag("settings[#{setting}][]", '').html_safe +
      choices.collect do |choice|
        text, value = (choice.is_a?(Array) ? choice : [choice, choice])
        content_tag(
          'p',
          content_tag(
            'label',
            check_box_tag(
              "settings[#{setting}][]",
              value,
              setting_values.include?(value),
              id: nil
            ) + text.to_s,
            class: (options[:inline] ? 'inline' : 'block')
          )
        )
      end.join.html_safe
  end

  def plugin_setting_text_field(setting, options = {})
    setting_label(setting, options).html_safe +
      text_field_tag("settings[#{setting}]", Setting.send("plugin_#{@plugin.id}")[setting], options).html_safe
  end

  def plugin_setting_check_box(setting, options = {})
    setting_label(setting, options).html_safe +
      check_box_tag("settings[#{setting}]", 1, Setting.send("plugin_#{@plugin.id}")[setting], options).html_safe
  end
end
