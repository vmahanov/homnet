class Rating < ActiveRecord::Base
  unloadable
  belongs_to :issue

   def self.gen_uniq_key
     o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
     (0...50).map { o[rand(o.length)] }.join
   end
end
