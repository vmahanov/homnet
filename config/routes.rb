# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

post '/issues/:id/send_rating_mail', :to => 'ratings#send_rating_mail'
get '/rating/:key', :to => 'ratings#set_rating'
get 'projects/:project_id/rating/report', :to => 'ratings#report'