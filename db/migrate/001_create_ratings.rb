class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.string :key
      t.integer :issue_id
      t.integer :rating
      t.datetime "created_on"
      t.datetime "updated_on"
    end
    add_index :ratings, :key, :unique => true
  end
end
