# frozen_string_literal: true

Redmine::Plugin.register :homnet do
  name 'Homnet plugin'
  author 'Mahanov Valeriy'
  description 'This is a plugin for Redmine'
  version '0.1.3'
  url 'https://bitbucket.org/vmahanov/homnet'
  author_url 'https://bitbucket.org/vmahanov'

  PLUGIN_PATH = Rails.root.join('plugins/homnet')

  require_dependency 'homnet_hook'
  require 'controller_issues_edit_before_save_hook'

  ActionDispatch::Callbacks.to_prepare do
    require_dependency 'mailer_patch'
    require_dependency 'issue_patch'
    require_dependency 'string_patch'
    require_dependency 'time_entry_query_patch'
  end

  settings default: { 'empty' => true }, partial: 'settings/homnet'
end

unless SettingsHelper.included_modules.include? HomnetSettingsHelper
  SettingsHelper.include HomnetSettingsHelper
end
