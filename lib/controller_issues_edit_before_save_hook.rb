#!/bin/env ruby
# encoding: utf-8
module Homnet
  class ControllerIssuesEditBeforeSaveHook < Redmine::Hook::ViewListener
    def controller_issues_edit_before_save(context={})
      if context[:issue] && (context[:issue].status.name == "Решена") && context[:issue].due_date.blank?
        context[:issue].due_date = Date.current
      end
    end
  end
end