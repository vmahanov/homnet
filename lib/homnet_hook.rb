class HomnetHook < Redmine::Hook::ViewListener

  render_on :view_issues_new_top, :partial => 'issues/view_issues_new_top'
  render_on :view_issues_show_details_bottom, :partial => 'issues/show/details_bottom'
  render_on :view_projects_show_right, :partial => "projects/show/right"

end