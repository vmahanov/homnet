#!/bin/env ruby
# frozen_string_literal: true

require_dependency 'issue'
require_dependency 'rating'

IssueQuery.available_columns << QueryColumn.new(:homnet_issues)

module Homnet
  module Patches
    module IssuePatch
      include IssuesHelper

      def self.included(base) # :nodoc:
        base.send(:include, InstanceMethods)
        base.class_eval do
          unloadable # Send unloadable so it will not be unloaded in development

          has_many :ratings, autosave: true, inverse_of: :issue, dependent: :destroy do
            def [](rating)
              find_by_rating(rating)
            end
          end
        end
      end

      module InstanceMethods
        def homnet_issues
          homnet_project_ids = Setting.plugin_homnet['project_ids'] ? Setting.plugin_homnet['project_ids'].reject(&:blank?).map(&:to_i) : []
          cf1 = IssueCustomField.find_by(id: Setting.plugin_homnet['homnet_hd_accept_field'])
          cf2 = IssueCustomField.find_by(id: Setting.plugin_homnet['homnet_hd_month_field'])
          return if homnet_project_ids.blank? || cf1.blank? || cf2.blank?

          joints = (relations_from.map(&:issue_to) + relations_to.map(&:issue_from)).reject { |i| i.project_id.in?(homnet_project_ids) }

          joints.map do |i|
            cfv1 = case i.custom_value_for(cf1)&.value
                   when 1, '1'
                     'Да'
                   when 0, '0'
                     'Нет'
                   else
                     ''
                   end
            "##{i.id}: #{cfv1} - #{i.custom_value_for(cf2)}"
          end.join('; ')
        end

        def gen_ratings
          # Теперь генерируем ключи для рейтингов
          ratings.destroy if ratings.any?
          (2..5).each do |i|
            ratings.create key: Rating.gen_uniq_key, rating: i
          end
        end

        def rating
          r = custom_value_for(CustomField.find_by_name('Оценка'))
          r = r.value if r
          r.blank? ? nil : r.to_i
        end

        def rating=(r)
          cf = CustomField.find_by_name('Оценка')
          self.custom_field_values = { cf.id.to_s => r.to_s }
          save
        end

        def tracker_name
          tracker.name
        end

        def limit_date
          custom_value_for(CustomField.find_by_name('Срок выполнения')).value
        end

        def time_spent
          total_spent_hours
        end

        def client_mail
          if assigned_to.roles_for_project(project).map(&:name).include?('Клиент')
            mail = assigned_to.mail
          elsif (parse = /<<(.*)>>/.match(custom_value_for(CustomField.find_by_name('Клиент')).value))
            mail = parse[1]
          else
            mail = nil
          end
          mail
        end
      end
    end
  end
end

unless Issue.included_modules.include?(Homnet::Patches::IssuePatch)
  Issue.include Homnet::Patches::IssuePatch
end
