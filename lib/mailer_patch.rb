module Homnet
  module Patches
    module MailerPatch

      def self.included(base) # :nodoc:
        base.send(:include, InstanceMethods)
        base.class_eval do
          unloadable # Send unloadable so it will not be unloaded in development

        end
      end

      module InstanceMethods
        def get_rating(issue)
          self.view_paths << "#{PLUGIN_PATH}/app/views/"
          @issue = Issue.find(issue)
          # Выясняем кому надо направить запрос оценки
          # Либо исполнителю, у которого роль "Клиент", либо тому кто указан в кастомном поле "Клиент"
          img_path = "#{PLUGIN_PATH}/assets/images/"
          attachments.inline['button_bad.png'] = File.read("#{img_path}button_bad.png")
          attachments.inline['button_normal.png'] = File.read("#{img_path}button_normal.png")
          attachments.inline['button_good.png'] = File.read("#{img_path}button_good.png")
          attachments.inline['button_perfect.png'] = File.read("#{img_path}button_perfect.png")
          mail(:to => @issue.client_mail, :from => Setting.mail_from, :subject => "[#{issue.project.name}]") do |format|
            format.html
          end
        end
      end
    end
  end
end

unless Mailer.included_modules.include?(Homnet::Patches::MailerPatch)
  Mailer.send(:include, Homnet::Patches::MailerPatch)
end
