#!/bin/env ruby
# frozen_string_literal: true

TimeEntryQuery.available_columns << QueryAssociationColumn.new(:issue, :id, :caption => :field_issue_id, :sortable => "#{Issue.table_name}.id")

